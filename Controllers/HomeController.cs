﻿using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using TestAppCSv.Data;
using TestAppCSv.Models;

namespace TestAppCSv.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        public HomeController(ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;

        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public async Task<IActionResult> InsertFromExcel(IFormFile theExcel)
        {
            ExcelPackage excel;

            using (var memorystream = new MemoryStream())
            {
                await theExcel.CopyToAsync(memorystream);
                excel = new ExcelPackage(memorystream);
            }

            var worksheet = excel.Workbook.Worksheets[1];
            var start = worksheet.Dimension.Start;
            var end = worksheet.Dimension.End;
            for (int row = start.Row; row <= end.Row; row++)
            {
                Staff a = new Staff
                {
                  //Id= int.Parse(worksheet.Cells[row, 1].Text),
                  Name= worksheet.Cells[row, 2].Text,
                  Sponsor= worksheet.Cells[row, 3].Text,
                  Bene= worksheet.Cells[row, 4].Text

                };

                _context.Staffs.Add(a);
            };
            _context.SaveChanges();
            return RedirectToAction(nameof(About));
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
