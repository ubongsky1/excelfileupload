﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAppCSv.Models
{
    public class Staff
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sponsor { get; set; }
        public string Bene { get; set; }
    }
}
